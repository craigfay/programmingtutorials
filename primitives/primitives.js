/**
 * What is 'primitive' data?
 * How should you conceptualize it?
 * Primitives vs. Objects in JS
 * The 3 Primitive Objects
 */

/**
 * Javascript's 5 Primitive Types
 * * boolean
 * * number
 * * string
 * * undefined
 * * null
 */