/**
 * Prerequisistes:
 * * Array.forEach
 * * rest, spread
 */

/**
* PART 1
* * Why they're important
* * Conceptualizing a linked list
* * Creating Nodes
* * Using Nodes to create a linked list
*/

/**
 * PART 2
 * * Find Length
 * * Searching
 * * Adding
 * * Deleting
 */

/**
 * When studying algorithms, retention isn't as important as practice
 */

/**
 * Linked Lists let interviewers test your ability to conceptualize data structures
 * They make you demonstrate your ability to consider performance tradeoffs
 */

/**
 * Talk about the tradeoffs of implementing functions different ways
 * Time efficient, or space efficient
 */

/**
 * Data can be anything: primitive data, or other structured data
 */

function createNode(data) {
  return {
    data: data,
    next: null,
  }
}

function listSearch(first, target) {
  let currentNode = first;
  while (currentNode) {
    if (currentNode.data == target) {
      return true;
    }
    currentNode = currentNode.next;
  }
  return false;
}

function linkedList(...data) {
  let [first, ...rest] = data;
  let firstNode = createNode(first);
  let lastNode = firstNode;
  
  rest.forEach(function(d) {
    lastNode.next = createNode(d);;
    lastNode = lastNode.next;
  })
  // This is the export object
  return {
    first: firstNode,
    length: length,
    add: add,
  }
}

function length() {
  let l = 1;
  let currentNode = this.first;
  while (currentNode.next) {
    l++;
    currentNode = currentNode.next;
  }
  return l;
}

function add(data) {
  let newNode = createNode(data);
  newNode.next = this.first;
  this.first = newNode;
}

let a = createNode('why');
let b = createNode('hello');
let c = createNode('there');

// Relationships are defined by using the `next` pointer
a.next = b;
b.next = c;

// The JSON representation of a linked list

let example = linkedList('good', 'day', 'to', 'you', 'sir');
example.add('fellow');

// let example = listSearch(a, 'goodbye');
let s = JSON.stringify(example, null, 4);
console.log(s);
